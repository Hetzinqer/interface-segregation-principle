package at.hakwt.swp4.isp.violated;

public class CarGarage implements Garage {
    @Override
    public void maintain(Car car) {
        // do maintenance work
    }

    @Override
    public void changeEngine(Car car) {
        // change the engine of the given car
    }

    @Override
    public void maintain(Bicycle bicycle) {
        throw new UnsupportedOperationException("Car garage cannot work with bicycles");
    }

    @Override
    public Bicycle sell() {
        throw new UnsupportedOperationException("Car garage cannot sell bicycles");
    }
}
