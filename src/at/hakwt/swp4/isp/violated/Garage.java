package at.hakwt.swp4.isp.violated;

public interface Garage {

    public void maintain(Car car);

    public void changeEngine(Car car);

    public void maintain(Bicycle bicycle);

    public Bicycle sell();

}
